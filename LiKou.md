# 力扣
*(每天可能更一题)*

## C++

**1. 两数之和**
```cpp
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        int i = 0;
        int ia = 0;
        for (i = 0; i < nums.size() - 1; i++){
            for (ia = i + 1; nums.size() != ia; ia++){
                if (nums[ia] + nums[i] == target){
                    vector<int>v = {i, ia} ;
                    return v;
                }
            }
        }
        return nums;
    }
};
```
**2. 两数相加**
```cpp
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        int i = 0;
        int ia = 0;
        for (i = 0; i < nums.size() - 1; i++){
            for (ia = i + 1; nums.size() != ia; ia++){
                if (nums[ia] + nums[i] == target){
                    vector<int>v = {i, ia} ;
                    return v;
                }
            }
        }
        return nums;
    }
};
```
**9. 回文数**
```cpp
class Solution {
public:
    bool isPalindrome(int x) {
        char str[100] = "";
        string strCmp;
        sprintf(str, "%d", x);
        for (int i = 0; str[i]; i++){
            strCmp.insert(strCmp.begin(), str[i]);
        }
        return !strcmp(str, strCmp.c_str());
    }
};
```
**14. 最长公共前缀**
```cpp
class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        string frontstr;
        int minsize = strs[0].size();
        int ia = 0;
        for (int i = 1; i < strs.size(); i++){
            if (strs[i].size() < minsize){
                minsize = strs[i].size();
            }
        }
        for (int i = 0; i < minsize; i++){
            for (ia = 0; ia < strs.size() - 1; ia++){
                if (strs[ia][i] != strs[ia + 1][i]){
                    return frontstr;
                }
            }
            frontstr.push_back(strs[0][i]);
        }
        return frontstr;
    }
};
```
**20. 有效的括号**
```cpp
class Solution {
public:
    bool isValid(string s) {
        stack<char>strS;
        for (char ch : s){
            if ('(' == ch || '[' == ch || '{' == ch){
                strS.push(ch);
            }
            else {
                switch(ch){
                case ')':
                    if (!strS.size() || '(' != strS.top()){
                        return false;
                    }
                    strS.pop();
                    break;
                case ']':
                    if (!strS.size() || '[' != strS.top()){
                        return false;
                    }
                    strS.pop();
                    break;
                case '}':
                    if (!strS.size() || '{' != strS.top()){
                        return false;
                    }
                    strS.pop();
                    break;
                default:
                    break;
                }
            }
        }
        return !strS.size();
    }
};
```
**26. 删除有序数组中的重复项**
```cpp
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        vector<int>v;
        for (int i = 0; i < nums.size(); i++){
            if (!i || nums[i - 1] != nums[i]){
                v.push_back(nums[i]);
            }
        }
        nums = v;
        return v.size(); 
    }
};
```
**27. 移除元素**
```cpp
class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        vector<int>v;
        int k = nums.size();
        for (int i : nums){
            if (val == i){
                k--;
            }
            else {
                v.push_back(i);
            }
        }
        nums = v;
        return k;
    }
};
```
**28. 找出字符串中第一个匹配项的下标**
```cpp
class Solution {
public:
    int strStr(string haystack, string needle) {
        if (nullptr == strstr(haystack.c_str(), needle.c_str())){
            return -1;
        }
        else {
            return strstr(haystack.c_str(), needle.c_str()) - haystack.c_str();
        }
    }
};
```
**58. 最后一个单词的长度**
```cpp
class Solution {
public:
    int lengthOfLastWord(string s) {
        int index = s.size() - 1; 
        int len = 0;
        while (0 <= index && ' ' == s[index]){ index--; }
        while (0 <= index && ' ' != s[index--]){ len++; }
        return len;
    }
};
```
**66. 加一**
```cpp
class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        digits[digits.size() - 1]++;
        int i = digits.size() - 1;
        while (digits[i] >= 10 && i >= 0){
            if (digits[i] >= 10){
                if (i - 1 >= 0){
                    digits[i - 1]++;
                }
                else {
                    digits[i] %= 10;
                    digits.push_back(0);
                    for (int ia = 0; ia < digits.size() - 2; ia++){
                        digits[ia + 1] = digits[ia];
                    }
                    digits[0] = 1;
                    break;
                }
                digits[i--] %= 10;
            }
        }
        return digits;
    }
};
```
**67. 二进制求和**
```cpp
class Solution {
public:
    string addBinary(string a, string b) {
        int addNum = 0;
        bool bItToEnd = false;
        bool bItaToEnd = false;
        bool addOne = false;
        string binStr;
        auto it = a.rbegin(); 
        auto ita = b.rbegin();
        while (it != a.rend() || ita != b.rend()) {
            addNum = (bItToEnd ? 0 : *it - '0') + (bItaToEnd ? 0 : *ita - '0') + addOne;
            binStr.insert(binStr.cbegin(), addNum % 2 + '0');
            addOne = addNum >= 2;
            if (a.rend() != it){
                it++;
            }
            if (b.rend() != ita){
                ita++;
            }
            if (b.rend() == ita) {
                bItaToEnd = true;
            }
            if (a.rend() == it) {
                bItToEnd = true;
            }
        }
        if (addOne){
            binStr.insert(binStr.cbegin(), '1');
        }
        return binStr;
    }
};
```
**94. 二叉树的中序遍历**
```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int>v;
        vector<int>returnV;
        if (nullptr == root){
            return v;
        }
        if (nullptr != root->left){
            returnV = inorderTraversal(root->left);
            for (int i : returnV){
                v.push_back(i);
            }
        }
        v.push_back(root->val);
        if (nullptr != root->right){
            returnV = inorderTraversal(root->right);
            for (int i : returnV){
                v.push_back(i);
            }
        }
        return v;
    }
};
```
**125. 验证回文串**
```cpp
class Solution {
public:
    bool isPalindrome(string s) {
        string ts;
        for (int i = 0; i < s.size(); i++){
            if (s[i] >= 'A' && s[i] <= 'Z'){
                ts.push_back(s[i] + 'a' - 'A');
            }
            else if(s[i] >= '0' && s[i] <= '9' || s[i] >= 'a' && s[i] <= 'z'){
                ts.push_back(s[i]);
            }
        }
        s = ts;
        string rs;
        for (int i = s.size() - 1; i >= 0; i--){
            rs.push_back(s[i]);
        }
        return !s.compare(rs);
    }
};
```
**136. 只出现一次的数字**
```cpp
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int num = 0;
        for (int i = 0; i < nums.size(); i++){
            num ^= nums[i];
        }
        return num;
    }
};
```
**141. 环形链表**
```cpp
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        if (nullptr == head){
            return false;
        }
        ListNode** slowPtr = &head->next;
        ListNode** fastPtr = &head;
        if (nullptr != *slowPtr){
            fastPtr = &head->next->next;
            while (nullptr != *fastPtr){
                slowPtr = &(*slowPtr)->next;
                fastPtr = &(*fastPtr)->next;
                if (nullptr != *fastPtr){
                    fastPtr = &(*fastPtr)->next;
                }
                if (*fastPtr == *slowPtr){
                    return true;
                }
            }
        }
        return false;
    }
};
```
**144. 二叉树的前序遍历**
```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int>v;
        vector<int>returnV;
        if (nullptr == root){
            return v;
        }
        v.push_back(root->val);
        if (nullptr != root->left){
            returnV = preorderTraversal(root->left);
            for (int i : returnV){
                v.push_back(i);
            }
        }
        if (nullptr != root->right){
            returnV = preorderTraversal(root->right);
            for (int i : returnV){
                v.push_back(i);
            }
        }
        return v;
    }
};
```

**145. 二叉树的后序遍历**
```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int>v;
        vector<int>returnV;
        if (nullptr == root){
            return v;
        }
        if (nullptr != root->left){
            returnV = postorderTraversal(root->left);
            for (int i : returnV){
                v.push_back(i);
            }
        }
        if (nullptr != root->right){
            returnV = postorderTraversal(root->right);
            for (int i : returnV){
                v.push_back(i);
            }
        }
        v.push_back(root->val);
        return v;
    }
};
```
**191. 位1的个数**
```cpp
class Solution {
public:
    int hammingWeight(int n) {
        int OneNum = 0;
        while (n){
            OneNum += n & 1;
            n /= 2;
        }
        return OneNum;
    }
};
```
**203. 移除链表元素**
```cpp
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val) {
        ListNode** searchNode = &head;
        while (nullptr != head && nullptr != *searchNode){
            if (val == (*searchNode)->val){
                *searchNode = (*searchNode)->next;
            }
            else {
                searchNode = &(*searchNode)->next;
            }
        }
        return head;
    }
};
```
**205. 同构字符串**
```cpp
class Solution {
public:
    bool isUnique(set<string>&s, char ch, bool isRight){
        for (string str : s){
            if (str[isRight] == ch){
                return false;
            }
        }
        return true;
    }
    bool isIsomorphic(string s, string t) {
        char beforeChs = 0;
        char beforeCht = 0;
        string str = "  ";
        set<string>se;
        map<char, int>m;
        for (int index = 0; index < s.size(); index++){
            str[0] = s[index];
            str[1] = t[index];
            m.insert({str[0], str[0] - str[1]});
            if (m[str[0]] != str[0] - str[1] || (1 == (beforeChs == s[index]) + (beforeCht == t[index])) || (1 == isUnique(se, s[index], false) + isUnique(se, t[index], true))){
                return false;
            }
            se.insert(str);
            beforeChs = s[index];
            beforeCht = t[index];
        }
        return true;
    }
};
```
**206. 反转链表**
```cpp
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        stack<int>s;
        while (nullptr != head){
            s.push(head->val);
            head = head->next;
        }
        ListNode** searchNode = &head;
        while (s.size()){
            *searchNode = new ListNode(s.top());
            s.pop();
            searchNode = &((*searchNode)->next);
        }
        return head;
    }
};
```
**217. 存在重复元素**
```cpp
class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        set<int>s;
        for (int i : nums){
            s.insert(i);
        }
        return s.size() != nums.size();
    }
};
```
**222. 完全二叉树的节点个数**
```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int countNodes(TreeNode* root) {
        return nullptr != root ? (countNodes(root->left) + countNodes(root->right) + 1) : 0;
    }
};
```
**231. 2 的幂**
```cpp
class Solution {
public:
    bool isPowerOfTwo(int n) {
        if (n < 0){
            return false;
        }
        int bitNum = 0;
        while (n){
            bitNum += n & 1;
            n /= 2;
        }
        return 1 == bitNum;
    }
};
```
**258. 各位相加**
```cpp
class Solution {
public:
    int addDigits(int num) {
        int i = 0;
        while (num){
            i += num % 10;
            num /= 10;
        }
        return i < 10 ? i : addDigits(i);
    }
};
```
**263. 丑数**
```cpp
class Solution {
public:
    bool isUgly(int n) {
        if (n <= 0){
            return false;
        }
        vector<int> arr = {2, 3, 5};
        int i = 0;
        while (1 != n){
            if (3 != i && !(n % arr[i])){
                n /= arr[i];
            }
            else if(3 == i){
                return false;
            }
            else{
                i++;
            }
        }
        return true;
    }
};
```
**278. 第一个错误的版本**
```cpp
// The API isBadVersion is defined for you.
// bool isBadVersion(int version);

class Solution {
public:
    int firstBadVersion(int n) {
        int s = 1;
        while (!isBadVersion(s)){
            s++;
        }
        return s;
    }
};
```
**326. 3 的幂**
```cpp
class Solution {
public:
    bool isPowerOfThree(int n) {
        long long ll = 1;
        while (n != ll){
            if (ll >= INT_MAX || n < ll){
                return false;
            }
            ll = ll * 3;
        }
        return true;
    }
};
```
**342. 4的幂**
```cpp
class Solution {
public:
    bool isPowerOfFour(int n) {
        if (n < 0){
            return false;
        }
        int bitNum = 0;
        bool check = false;
        while (n){
            if (check && n & 1){
                return false;
            }
            else {
                bitNum += n & 1;
            }
            n /= 2;
            check = !check;
        }
        return 1 == bitNum;
    }
};
```
**344. 反转字符串**
```cpp
class Solution {
public:
    void reverseString(vector<char>& s) {
        auto it = s.begin();
        auto rit = s.end() - 1;
        char ch = 0;
        for (; rit > it; it++, rit--){
            ch = *it;
            *it = *rit;
            *rit = ch;
        }
    }
};
```
**414. 第三大的数**
```cpp
class Solution {
public:
    int thirdMax(vector<int>& nums) {
        set<int, greater<int>>s;
            for (int i : nums){
                s.insert(i);
            }
        if (2 >= s.size()){
            return *s.begin();
        }
        else{
            auto it = s.begin();
            it++;
            return *++it;
        }
    }
};
```
**1299. 将每个元素替换为右侧最大元素**
```cpp
class Solution {
public:
    vector<int> replaceElements(vector<int>& arr) {
        int max = 0;
        int ia = 0;
        for (int i = 0; i < arr.size() - 1; i++){
            for (max = arr[i + 1], ia = i + 1; ia < arr.size(); ia++){
                max < arr[ia] ? max = arr[ia] : 0;
            }
            arr[i] = max;
        }
        arr.back() = -1;
        return arr;
    }
};
```
**2235. 两整数相加**
```cpp
class Solution {
public:
    int sum(int num1, int num2) {
        return num1 + num2;
    }
};
```

## shell

**192. 统计词频**
```shell
cat words.txt | tr -s ' ' '\n' | sort | uniq -c | sort -r | awk '{printf("%s %s\n", $2, $1)}'
```

**193. 有效电话号码**
```shell
awk '/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/' file.txt
```

**194. 转置文件**
```shell
line=`head -n 1 file.txt | wc -w`
for ((i=1;i<=$line;i++))
do
    awk '{print $'$i'}' file.txt | xargs
done
```

**195. 第十行**
```shell
sed -n '10p' file.txt
```

## SQL

**176. 第二高的薪水**
```sql
SELECT 
    IF(
    (SELECT COUNT(DISTINCT(salary))
    FROM Employee) >= 2, 

    (SELECT DISTINCT(salary)
    FROM Employee
    ORDER BY salary DESC
    LIMIT 1, 1),

    null) SecondHighestSalary;
```
**182. 查找重复的电子邮箱**
```sql
SELECT email Email FROM Person GROUP BY email HAVING count(email) >= 2;
```